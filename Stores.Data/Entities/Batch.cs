﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stores.Domain.Entities
{
    public class Batch
    {
        public int BatchId { get; set; }

        public int StoreRoomId { get; set; }
        public StoreRoom StoreRoom { get; set; }

        public int ItemId { get; set; }
        public Item Items { get; set; }

        public string BatchNumber { get; set; }
        public string ItemLocation { get; set; }
        public DateTime CheckedDate { get; set; }

        public int UserId { get; set; } 
        public User Users { get; set; }

        public ICollection<BatchExpiryDate> BatchExpiryDates { get; set; }
        public ICollection<ItemSerialNumber> ItemSerialNumbers { get; set; }
    }
}
