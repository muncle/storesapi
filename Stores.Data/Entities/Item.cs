﻿using System;
using System.Collections.Generic;

namespace Stores.Domain.Entities
{
    public class Item
    {
        public int ItemId { get; set; }
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string ItemRemarks { get; set; }

        public ICollection<Batch> Batches { get; set; }
    }
}
