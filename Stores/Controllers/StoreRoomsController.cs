﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Stores.Domain.Entities;
using Stores.Persistence;

namespace Stores.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreRoomsController : ControllerBase
    {
        private readonly StoresDbContext _context;

        public StoreRoomsController(StoresDbContext context)
        {
            _context = context;
        }

        // GET: api/StoreRooms
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StoreRoom>>> GetStoreRooms()
        {
            return await _context.StoreRooms.ToListAsync();
        }

        // GET: api/StoreRooms/5
        [HttpGet("{id}")]
        public async Task<ActionResult<StoreRoom>> GetStoreRoom(int id)
        {
            var storeRoom = await _context.StoreRooms.FindAsync(id);

            if (storeRoom == null)
            {
                return NotFound();
            }

            return storeRoom;
        }

        // PUT: api/StoreRooms/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStoreRoom(int id, StoreRoom storeRoom)
        {
            if (id != storeRoom.StoreRoomId)
            {
                return BadRequest();
            }

            _context.Entry(storeRoom).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoreRoomExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/StoreRooms
        [HttpPost]
        public async Task<ActionResult<StoreRoom>> PostStoreRoom(StoreRoom storeRoom)
        {
            _context.StoreRooms.Add(storeRoom);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStoreRoom", new { id = storeRoom.StoreRoomId }, storeRoom);
        }

        // DELETE: api/StoreRooms/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<StoreRoom>> DeleteStoreRoom(int id)
        {
            var storeRoom = await _context.StoreRooms.FindAsync(id);
            if (storeRoom == null)
            {
                return NotFound();
            }

            _context.StoreRooms.Remove(storeRoom);
            await _context.SaveChangesAsync();

            return storeRoom;
        }

        private bool StoreRoomExists(int id)
        {
            return _context.StoreRooms.Any(e => e.StoreRoomId == id);
        }
    }
}
