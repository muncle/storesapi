﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Stores.Domain.Entities;

namespace Stores.Persistence.Configurations
{
    public class StoreRoomConfiguration : IEntityTypeConfiguration<StoreRoom>
    {
        public void Configure(EntityTypeBuilder<StoreRoom> builder)
        {
            builder.HasKey(e => e.StoreRoomId);

        }
    }
}
