﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Stores.Persistence.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StoreRooms",
                columns: table => new
                {
                    StoreRoomId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Store = table.Column<string>(nullable: true),
                    StoreName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreRooms", x => x.StoreRoomId);
                });

            migrationBuilder.CreateTable(
                name: "PartNumbers",
                columns: table => new
                {
                    ItemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ItemNumber = table.Column<string>(nullable: true),
                    ItemDescription = table.Column<string>(nullable: true),
                    ItemRemarks = table.Column<string>(nullable: true),
                    StoreRoomId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartNumbers", x => x.ItemId);
                    table.ForeignKey(
                        name: "FK_PartNumbers_StoreRooms_StoreRoomId",
                        column: x => x.StoreRoomId,
                        principalTable: "StoreRooms",
                        principalColumn: "StoreRoomId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Batches",
                columns: table => new
                {
                    BatchId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StoreRoomId = table.Column<int>(nullable: false),
                    ItemId = table.Column<int>(nullable: false),
                    BatchNumber = table.Column<string>(nullable: true),
                    ItemLocation = table.Column<string>(nullable: true),
                    CheckedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Batches", x => x.BatchId);
                    table.ForeignKey(
                        name: "FK_Batches_PartNumbers_ItemId",
                        column: x => x.ItemId,
                        principalTable: "PartNumbers",
                        principalColumn: "ItemId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Batches_StoreRooms_StoreRoomId",
                        column: x => x.StoreRoomId,
                        principalTable: "StoreRooms",
                        principalColumn: "StoreRoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BatchExpiryDates",
                columns: table => new
                {
                    BatchExpiryDateId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ItemId = table.Column<int>(nullable: false),
                    BatchId = table.Column<int>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchExpiryDates", x => x.BatchExpiryDateId);
                    table.ForeignKey(
                        name: "FK_BatchExpiryDates_Batches_BatchId",
                        column: x => x.BatchId,
                        principalTable: "Batches",
                        principalColumn: "BatchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemSerialNumbers",
                columns: table => new
                {
                    ItemSerialNumberId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BatchId = table.Column<int>(nullable: false),
                    SerialNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemSerialNumbers", x => x.ItemSerialNumberId);
                    table.ForeignKey(
                        name: "FK_ItemSerialNumbers_Batches_BatchId",
                        column: x => x.BatchId,
                        principalTable: "Batches",
                        principalColumn: "BatchId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Batches_ItemId",
                table: "Batches",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Batches_StoreRoomId",
                table: "Batches",
                column: "StoreRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_BatchExpiryDates_BatchId",
                table: "BatchExpiryDates",
                column: "BatchId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemSerialNumbers_BatchId",
                table: "ItemSerialNumbers",
                column: "BatchId");

            migrationBuilder.CreateIndex(
                name: "IX_PartNumbers_StoreRoomId",
                table: "PartNumbers",
                column: "StoreRoomId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BatchExpiryDates");

            migrationBuilder.DropTable(
                name: "ItemSerialNumbers");

            migrationBuilder.DropTable(
                name: "Batches");

            migrationBuilder.DropTable(
                name: "PartNumbers");

            migrationBuilder.DropTable(
                name: "StoreRooms");
        }
    }
}
