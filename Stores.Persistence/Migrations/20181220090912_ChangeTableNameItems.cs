﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Stores.Persistence.Migrations
{
    public partial class ChangeTableNameItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Batches_PartNumbers_ItemId",
                table: "Batches");

            migrationBuilder.DropForeignKey(
                name: "FK_PartNumbers_StoreRooms_StoreRoomId",
                table: "PartNumbers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PartNumbers",
                table: "PartNumbers");

            migrationBuilder.RenameTable(
                name: "PartNumbers",
                newName: "Items");

            migrationBuilder.RenameIndex(
                name: "IX_PartNumbers_StoreRoomId",
                table: "Items",
                newName: "IX_Items_StoreRoomId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Items",
                table: "Items",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Batches_Items_ItemId",
                table: "Batches",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "ItemId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_StoreRooms_StoreRoomId",
                table: "Items",
                column: "StoreRoomId",
                principalTable: "StoreRooms",
                principalColumn: "StoreRoomId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Batches_Items_ItemId",
                table: "Batches");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_StoreRooms_StoreRoomId",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Items",
                table: "Items");

            migrationBuilder.RenameTable(
                name: "Items",
                newName: "PartNumbers");

            migrationBuilder.RenameIndex(
                name: "IX_Items_StoreRoomId",
                table: "PartNumbers",
                newName: "IX_PartNumbers_StoreRoomId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PartNumbers",
                table: "PartNumbers",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Batches_PartNumbers_ItemId",
                table: "Batches",
                column: "ItemId",
                principalTable: "PartNumbers",
                principalColumn: "ItemId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PartNumbers_StoreRooms_StoreRoomId",
                table: "PartNumbers",
                column: "StoreRoomId",
                principalTable: "StoreRooms",
                principalColumn: "StoreRoomId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
