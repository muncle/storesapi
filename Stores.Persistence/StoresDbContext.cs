﻿using System;
using Microsoft.EntityFrameworkCore;
using Stores.Domain.Entities;

namespace Stores.Persistence
{
    public class StoresDbContext : DbContext
    {
        public StoresDbContext(DbContextOptions<StoresDbContext> options) : base(options)
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<BatchExpiryDate> BatchExpiryDates { get; set; }
        public DbSet<ItemSerialNumber> ItemSerialNumbers { get; set; }
        public DbSet<StoreRoom> StoreRooms { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(StoresDbContext).Assembly);
        }
    }
}
